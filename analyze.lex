%{
#include "tokens.h"
#include "y.tab.h"
%}

%option yylineno

%%

"/*"([^*]|\*+[^*/])*\*+"/" { /*ignore comments*/ }

"let" {
  //printf("[reserved, let]\n");
  t = t_let;
  strncpy (token_data, yytext, TAM_TOKEN);
  return T_LET;
}

"end" {
  //printf("[reserved, end]\n");
  t = t_end;
  strncpy (token_data, yytext, TAM_TOKEN);
  return T_END;
}

"int" {
  //printf("[reserved, int]\n");
  t = t_integer;
  strncpy (token_data, yytext, TAM_TOKEN);
  return INTEGER;
}

"string" {
  //printf("[reserved, string]\n");
  t = t_string;
  strncpy (token_data, yytext, TAM_TOKEN);
  return STRING;
}

"float" {
  //printf("[reserved, float]\n");
  t = t_float;
  strncpy (token_data, yytext, TAM_TOKEN);
  return FLOAT;
}

"boolean" {
  //printf("[reserved, boolean]\n");
  t = t_boolean;
  strncpy (token_data, yytext, TAM_TOKEN);
  return BOOLEAN;
}

"for" {
  //printf("[reserved, for]\n");
  t = t_for;
  strncpy (token_data, yytext, TAM_TOKEN);
  return FOR;
}

"to" {
  //printf("[reserved, to]\n");
  t = t_to;
  strncpy (token_data, yytext, TAM_TOKEN);
  return TO;
}

"break" {
  //printf("[reserved, break]\n");
  t = t_break;
  strncpy (token_data, yytext, TAM_TOKEN);
  return BREAK;
}

"true" { 
  //printf("[logical, true]\n");
  t = t_true;
  strncpy (token_data, yytext, TAM_TOKEN);
  return TRUE;
}

"false" { 
  //printf("[logical, false]\n");
  t = t_false;
  strncpy (token_data, yytext, TAM_TOKEN);
  return FALSE;
}

"var" { 
  //printf("[reserved, var]\n");
  t = t_var;
  strncpy (token_data, yytext, TAM_TOKEN);
  return VAR;
}

"function" {
  //printf("[reserved, function]\n");
  t = t_function;
  strncpy (token_data, yytext, TAM_TOKEN);
  return FUNCTION;
}

"if" {
  //printf("[reserved, if]\n");
  t = t_if;
  strncpy (token_data, yytext, TAM_TOKEN);
  return IF;
}

"then" {
  //printf("[reserved, then]\n");
  t = t_then;
  strncpy (token_data, yytext, TAM_TOKEN);
  return THEN;
}

"else" {
  //printf("[reserved, else]\n");
  t = t_else;
  strncpy (token_data, yytext, TAM_TOKEN);
  return ELSE;
}

"while" {
  //printf("[reserved, while]\n");
  t = t_while;
  strncpy (token_data, yytext, TAM_TOKEN);
  return WHILE;
}

"do" {
  //printf("[reserved, do]\n");
  t = t_do;
  strncpy (token_data, yytext, TAM_TOKEN);
  return DO;
}

"in" {
  //printf("[reserved, in]\n");
  t = t_in;
  strncpy (token_data, yytext, TAM_TOKEN);
  return T_IN;
}

[0-9]+  {
  //printf("[num, %s]\n", yytext);
  t = t_num;
  strncpy (token_data, yytext, TAM_TOKEN);
  return NUM;
}

[a-zA-Z][a-zA-Z0-9_]* {
  //printf("[id, %s]\n", yytext);
  t = t_id;
  strncpy (token_data, yytext, TAM_TOKEN);
  return ID;
}

":=" {
  //printf("[attr, :=]\n");
  t = t_attr;
  strncpy (token_data, yytext, TAM_TOKEN);
  return ATTR;
}

":" {
  //printf("[type_def, :]\n");
  t = t_col;
  strncpy (token_data, yytext, TAM_TOKEN);
  return COL;
}

";" {
  //printf("[scollum, ;]\n");
  t = t_scollum;
  strncpy (token_data, yytext, TAM_TOKEN);
  return SCOLLUM;
}

"," {
  //printf("[comma, ,]\n");
  t = t_comma;
  strncpy (token_data, yytext, TAM_TOKEN);
  return COMMA;
}

"(" {
  //printf("[oparenthesis, (]\n");
  t = t_oparenthesis;
  strncpy (token_data, yytext, TAM_TOKEN);
  return OPARENTHESIS;
}

")" {
  //printf("[cparenthesis, )]\n");
  t = t_cparenthesis;
  strncpy (token_data, yytext, TAM_TOKEN);
  return CPARENTHESIS;
}

"+" {
  //printf("[op, +]\n");
  t = t_sum;
  strncpy (token_data, yytext, TAM_TOKEN);
  return SUM;
}

"-" {
  //printf("[op, -]\n");
  t = t_minus;
  strncpy (token_data, yytext, TAM_TOKEN);
  return MINUS;
}

"*" {
  //printf("[op, *]\n");
  t = t_mul;
  strncpy (token_data, yytext, TAM_TOKEN);
  return MUL;
}

"/" {
  //printf("[op, /]\n");
  t = t_div;
  strncpy (token_data, yytext, TAM_TOKEN);
  return DIV;
}

"=" {
  //printf("[op, =]\n");
  t = t_eq;
  strncpy (token_data, yytext, TAM_TOKEN);
  return EQ;
}

"<>" {
  //printf("[op, <>]\n");
  t = t_diff;
  strncpy (token_data, yytext, TAM_TOKEN);
  return DIFF;
}

">" {
  //printf("[op, >]\n");
  t = t_greater;
  strncpy (token_data, yytext, TAM_TOKEN);
  return GREATER;
}

"<" {
  //printf("[op, <]\n");
  t = t_less;
  strncpy (token_data, yytext, TAM_TOKEN);
  return LESS;
}

">=" {
  //printf("[op, >=]\n");
  t = t_greater_equal;
  strncpy (token_data, yytext, TAM_TOKEN);
  return GREATER_EQUAL;
}

"<=" {
  //printf("[op, <=]\n");
  t = t_less_equal;
  strncpy (token_data, yytext, TAM_TOKEN);
  return LESS_EQUAL;
}

"&" {
  //printf("[op, &]\n");
  t = t_and;
  strncpy (token_data, yytext, TAM_TOKEN);
  return AND;
}

"|" {
  //printf("[op, |]\n");
  t = t_or;
  strncpy (token_data, yytext, TAM_TOKEN);
  return OR;
}

L?\"(\\.|[^\\"])*\" {
  //printf("[literal, %s]\n", yytext);
  t = t_literal;
  strncpy (token_data, yytext, TAM_TOKEN);
  return LITERAL;
}

.|\n    {   /* Ignore all other characters. */   }
%%