%{
    #include "tokens.h"
%}

%error-verbose
%token VAR FUNCTION IF THEN ELSE DO WHILE FOR
%token T_LET T_IN T_END TO BREAK
%token NUM ID LITERAL
%token ATTR SCOLLUM COMMA OPARENTHESIS CPARENTHESIS
%token SUM MINUS MUL DIV
%token EQ DIFF GREATER LESS GREATER_EQUAL LESS_EQUAL
%token OR AND
%token TRUE FALSE
%token COL
%token INTEGER BOOLEAN STRING FLOAT

%%

program:    line_list
    |       let
;

epsilon:
;

exp_block:  line                           
    |       OPARENTHESIS line_list CPARENTHESIS
;

line_list:  line   
        |   line line_list
;

line: command
    | command SCOLLUM
;

command:    attrib          
    |       var_declare     
    |       if_then         
    |       if_then_else    
    |       while           
    |       for             
    |       function_call   
;

exp:    expression              
    |   logical_expression      
    |   relation_expression     
;

expression: expression SUM term     
    |       expression MINUS term   
    |       term                    
;

term:   term MUL factor
    |   term DIV factor
    |   factor
;

factor:     NUM
    |       id_or_function_call
    |       OPARENTHESIS expression CPARENTHESIS
;

logical_expression:     logical_expression AND logical_factor   
    |                   logical_expression OR logical_factor    
    |                   logical_factor                          
;

logical_factor:     TRUE
    |               FALSE
    |               id_or_function_call
    |               OPARENTHESIS logical_expression CPARENTHESIS
    |               relation_expression
    |               expression
;

relation_expression:    relation_expression LESS relation_factor            
    |                   relation_expression LESS_EQUAL relation_factor      
    |                   relation_expression GREATER relation_factor         
    |                   relation_expression GREATER_EQUAL relation_factor   
    |                   relation_expression EQ relation_factor              
    |                   relation_expression DIFF relation_factor            
    |                   relation_factor                                     
;

relation_factor:    NUM
    |               id_or_function_call
    |               OPARENTHESIS relation_expression CPARENTHESIS
    |               logical_expression
    |               expression
;

type:   INTEGER 
    |   FLOAT   
    |   BOOLEAN 
    |   STRING  
;

fields: field_declare
    |   epsilon
;

fields_call:    field_call_declare
    |           epsilon
;

field_call_declare:     id_or_function_call
            |           NUM
            |           LITERAL
            |           id_or_function_call COMMA field_call_declare
            |           NUM COMMA field_call_declare
            |           LITERAL COMMA field_call_declare
;

field_declare:  ID COL type
            |   ID COL type COMMA field_declare
;

function_declare:   FUNCTION ID OPARENTHESIS fields CPARENTHESIS EQ function_body           
    |               FUNCTION ID OPARENTHESIS fields CPARENTHESIS COL type EQ function_body  
;

function_body:  NUM
            |   exp_block
;

function_call: ID OPARENTHESIS fields_call CPARENTHESIS
;

id_or_function_call:    ID
                |       function_call
;

var_declare:    VAR var_list
            |   VAR attrib
;

var_list:   ID
        |   ID COMMA var_list
;

attrib:     ID ATTR exp
        |   ID ATTR LITERAL
;

if_then:    IF exp THEN exp_block
;

if_then_else:     IF exp THEN exp_block ELSE exp_block
;

while:  WHILE exp DO exp_block
;

for:  FOR ID ATTR exp TO exp DO exp_block
;

let:    T_LET decs T_IN line_list T_END
;

decs:   dec
    |   epsilon
    |   dec decs 
;

dec:    var_declare
    |   var_declare SCOLLUM
    |   function_declare SCOLLUM
    |   function_declare
;

%%

int main (void) { yyparse(); }

void yyerror(char *s){
    extern int yylineno;
    fprintf (stderr, "%s on line %d\n", s, yylineno);
}
