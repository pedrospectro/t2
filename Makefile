all: tc

lex.yy.c: analyze.lex
	lex analyze.lex

y.tab.c: lex.yy.c
	yacc -d analyze.y

tc: y.tab.c
	cc -o tc y.tab.c lex.yy.c -ly -ll

clean:
	rm y.* && rm *.c && rm tc
