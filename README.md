Professor: André Guedes
Aluno: Pedro Henrique C Sá 
login: phcs09
GRR20094606

Objetivo: Contruir um analisador sintático para a linguagem Tiger-- usando lex e yacc (ou flex e bison).

A linguagem Tiger-- é um subconjunto da linguagem Tiger, definida no livro Modern Compiler Implementation in C de Andrew W. Appel. Disponível em http://www.cs.princeton.edu/~appel/modern/.

Entrada: um arquivo texto

Saída: Nada, caso a entrada seja um programa válido para a linguagem Tiger--. Caso a entrada não seja um programa válido, escrever algo que indique isso, dizendo o motivo (mensagem de erro correspondente).

Execução do Programa: Entrada padrão e saída padrão. (Filtro)

Nome do executável: tc--

Arquivos: Makefile e fontes (lex e yacc) tudo dentro de um tar.gz com o nome do seu login.

Correção:

1a Fase: Compilar
2a Fase: Rodar com 1 exemplo simples.
3a Fase: Rodar com mais exemplos e verificar a saída.
4a Fase: olhar o fonte.
Observação: Não é necessário nenhuma semântica.

Para rodar:
    1) make
    2) ./tc < arquivo.tiger
